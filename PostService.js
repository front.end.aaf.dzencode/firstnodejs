import Post from "./post.js";
import FileService from "./FileService.js";

class PostService {// такой сервис нужен для отделения слоя логики контроллера от сервиса
    async create(post, picture) {
            const fileName = FileService.saveFile(picture);
            const createdPost = await Post.create({...post, picture: fileName});
            //передаем в схему нужные поля. + сохраняем все в базу mongoDB
            return createdPost;
    };

    async getAll() {
            const posts = await Post.find();
            //обращаемся к схеме Post, метод find найдет все что там лежит в монгоДБ (автоматом)
            return posts;
            // возвращаем посты в контроллер
    };

    async getOne(id) {
            if (!id) {
                throw new Error('Не указан ID');
            }
            const post = await Post.findById(id);
            // Post.findById(id) метод у схемы который автоматом вернет пост по id из базы монгоДБ
            return post;
    };

    async update(post) {
        if (!post._id) {
            throw new Error('Не указан ID');
        }
            const updatedPost = await Post.findByIdAndUpdate(post._id, post, {new: true});
            //Post.findByIdAndUpdate метод у схемы который найдет в базе пост по id,обновит его
            // и вернет его новое состояние
            return updatedPost;
    };
    async delete(id) {
        if (!id) {
            throw new Error('Не указан ID');
        }
            const post = await Post.findByIdAndDelete(id);
            return post;

    };
};

export default new PostService();
