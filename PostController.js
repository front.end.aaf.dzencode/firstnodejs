import Post from "./post.js";
import PostService from "./PostService.js";

//PostService отделил логику сервиса от логики контроллера
class PostController {//нужен для отделения логики от роутеров
    async create(req, res) {
        try{
             console.log(req.files.picture);//файлы запроса, сначала нужно установить и зарегать (express-fileupload)
            // console.log(req.body);//тело запроса (у пост!)
            const post = await PostService.create(req.body, req.files.picture);
            //передаем в PostService тело запроса. + PostService сохранит все в базу mongoDB
            res.status(200).json(post)
        } catch(err){
            res.status(500).json(err)
        }
    }
    async getAll(req, res){
        try{
            const posts = await PostService.getAll();
            //обращаемся к PostService, метод getAll
            return res.json(posts);
            // возвращаем посты на клиент в ответе
        }catch (err){
            res.status(500).json(err)
        }
    }
    async getOne(req, res){
        try{
            const {id} = req.params
            const post = await PostService.getOne(id);
            // передали в PostService id
            return res.json(post);
        }catch (err){
            res.status(500).json(err)
        }
    }
    async update(req, res){
        try{
            const post = req.body;
            const updatedPost = await PostService.update(post);
            return res.json(updatedPost);
        }catch (err){
            res.status(500).json(err.message)
        }
    }
    async delete(req, res){
        try{
            const {id} = req.params;
            const post = await PostService.delete(id);
            return res.json(post);
        }catch (err){
            res.status(500).json(err)
        }
    }
}
export default  new PostController();
