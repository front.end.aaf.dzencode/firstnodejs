import mongoose from "mongoose";

const Post = new mongoose.Schema({
    author: {type:String, required:true},
    title: {type:String, required:true},
    content: {type:String, required:true},
    picture: {type: String},
});
//создание схемы для пост запроса с типами данных которые запрос принимает и их required

export default  mongoose.model('Post', Post);