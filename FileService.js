import * as uuid from 'uuid';
import * as path from "path";

class FileService{
     saveFile(file) {
        try {
            const fileName = uuid.v4() + '.jpg';//создать файл
            const filePath = path.resolve('static', fileName);
            //static - название папки
            file.mv(filePath);//переместить файл по пути
            return fileName;
        } catch(e) {
            console.log(e);
        }

    }
}
export default new FileService();
