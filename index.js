import express from 'express';
import mongoose from 'mongoose';//database
import router from "./router.js"; //импортируем роутер
import fileUpload from 'express-fileupload';

const PORT = 5000;
const DB_URL = 'mongodb+srv://user:user@cluster0.zqko6.mongodb.net/myFirstDatabase?retryWrites=true&w=majority';
//DB_URL берем из mongoose, создаем там проект,выбираем подключение к апп копируем ссылку вставляем пароль

const app = express();
app.use(express.json());
app.use(express.static('static'));//позволяет по url в корне отдать файл по имени из папки static
app.use(fileUpload({}));
app.use('/api', router) // регистрация роутов в апи с путем /api
//модули через use в рамках express считаются middleware
// app.get('/', (req,res)=>{
//     console.log(req.query);//квери параметры ?id=1
//     res.status(200).json('сервер запущен__')
// }) пример отправки без роутера и без монго. (остальное в router.js)

async function startApp(uri, callback) {
    try{
        await mongoose.connect(DB_URL,
            {useUnifiedTopology: true, useNewUrlParser: true});
        app.listen(PORT, ()=> console.log(`start at PORT: ${PORT}`))
    } catch(err){
        console.log(err)
    }
}

startApp();

