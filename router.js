import Router from 'express';
import PostController from "./PostController.js";

const router = new Router();
//в роутере описываем маршруты запросов + их логику описываем в postController.js (это эндпоинты)
router.post('/posts',PostController.create);
//PostController.create использование отделенной логики этого запроса
router.get('/posts', PostController.getAll);
router.get('/posts/:id', PostController.getOne);
router.put('/posts', PostController.update);
router.delete('/posts/:id', PostController.delete);

export default router;
